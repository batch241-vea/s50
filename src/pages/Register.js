import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';



export default function Register() {

    //useContext() - Allows us to use the UserContext object and its properties to be used for user validation.
    const {user, setUser} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);
    const navigate = useNavigate();


    //Function to simulate user registration
    function registerUser() {
        
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data){
                Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Zuitt"              
                })
                setFirstName('');
                setLastName('');
                setEmail('');
                setPassword1('');
                setPassword2('');
                setMobileNo('');
                setIsActive(false);
                navigate("/login")
            } else {
                Swal.fire({
                    title: "Registration failed",
                    icon: "error",
                    text: "Please try again"
                })

            }

            
        })

    //alert('Thank you for registering!') 
        
    }
               
    const emailExist = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({email})
        })
        .then(res => res.json())
        .then(data => {
            if (data === true){
                Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide a different email!"
            })

            } else {
                registerUser();
            }
        })
    }


    useEffect(()=> {
        // Validation to enable submit button when all fields are populated and both password match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && mobileNo.length > 10 && password1 !== '' && password2 !== '')&&(password1 === password2)){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    // Dependencies
    // No dependencies - effect function will run every time component renders
    // With dependency (empty array) - effect function will only run (one time) when the component renders
    // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return (
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={emailExist}>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name"
                    value = {firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name"
                    value = {lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter number"
                    value = {mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value = {password2}
                    onChange = {e => setPassword2(e.target.value)} 
                    required
                />
            </Form.Group><br/>

            {/*conditionally render submit button based on "isActive" state*/}
            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
        </Form>
    )
}

