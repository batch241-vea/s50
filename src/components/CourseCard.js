import { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CourseCard({courseProps}) {

  //[ACTIVITY]
  const { name, description, price, _id } = courseProps;

  //Use the state hook for this component to be able to store its state
  //States are used to keep track of information realated to individual components
  /*
    Syntax
    const [getter, setter] = useState(initialGetterValue)
  */

  //const [seatState, setSeatState] = useState(30);
  //const [count, setCount] = useState(0);
  //const [setIsOpen] = useState(true);

  //console.log(useState);

  /*function enroll(){
      setSeatState(seatState - 1);   
      setCount(count + 1);*/

/*    if (seatState > 0){
      setSeatState(seatState - 1);   
      setCount(count + 1);
      
    } else {
      alert("No more seats");
    } 
  }*/
  //useEffect() - will allow us to execute a function if the value of seatState changes
  /*useEffect(() => {
    if(seatState === 0){
      setIsOpen(false);
      document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
    }
    // will run anytime one if the values in the array of the dependencies changes
    //[] run only once
  }, [seatState, id, setIsOpen])
*/

  return (
  <Card>
      <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}.</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          {/*<Card.Text>Enrollees: {count}</Card.Text>
          <Card.Text>Seats: {seatState}</Card.Text>*/}
          {/*<Button variant="primary">Enroll</Button>*/}
          {/*<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}

          <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>

      </Card.Body>
  </Card>
  )

}